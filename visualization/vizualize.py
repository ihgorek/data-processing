import os
import pandas as pd


def eq(path):
    df = pd.read_csv(path, sep=';')
    single_data = pd.DataFrame(df.loc[df['Dividing strip'] == 'DOUBLE'])
    print(single_data)
    road_list = single_data['Road'].tolist()
    car_list = single_data['O311KO'].tolist()
    count = 0
    for i in range(len(road_list)):
        if road_list[i] == car_list[i]:
            count += 1
    print(count)


if __name__ == '__main__':
    path = os.getcwd()
    file_name = 'test.csv'
    path_to_file = os.path.join(path, file_name)
    eq(path_to_file)
