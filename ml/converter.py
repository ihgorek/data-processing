import os 
import sys

def main():
	path = os.getcwd()
	file_name = 'test.csv'
	path_to_file = os.path.join(path, file_name)
	path_to_new_file = os.path.join(path, 'new_test.csv')

	with open(path_to_file, 'r') as old:
		with open(path_to_new_file, 'w') as new:
			for row in old:
				new_row = row.replace(',','.')
				print(new_row)
				new.write(new_row)



if __name__ == '__main__':
	main()