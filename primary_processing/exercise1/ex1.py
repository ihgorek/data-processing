import os
import csv


class ScheduleAnalysis(object):

    def __init__(self, path_to_csv):
        self.schedule_times = []
        self.avr = {}
        with open(path_to_csv, 'r') as csv_obj:
            self.schedule = csv.reader(csv_obj)

            for row in self.schedule:
                one_times = []
                count = 0
                for time in row:
                    count += 1
                    time_in_seconds = self.convert_to_sec(time)
                    one_times.append(time_in_seconds)
                    print(time_in_seconds, end=', ')
                    if count > 2:
                        break
                print('')
                self.schedule_times.append(one_times)

    def convert_to_average(self, num_of_row):
        if num_of_row == len(self.schedule_times) - 1:
            print('please entered any value')
            return 0
        first_row = self.schedule_times[num_of_row]
        second_row = self.schedule_times[num_of_row + 1]
        average_time = 0
        for i in range(len(first_row)):
            time_first_stop = first_row[i]
            time_second_stop = second_row[i]
            average_time += abs(time_second_stop - time_first_stop)
            print(time_first_stop, time_second_stop)
        print(average_time, average_time/len(first_row))
        return average_time/len(first_row)

    def add_to_avr_struct(self, num, average):
        self.avr['Среднее ' + str(num)] = average
        print(self.avr)

    def sec_to_time(self, time, avr):
        time_in_seconds = self.convert_to_sec(time)
        time_in_seconds += avr
        ans_time = self.convert_of_sec(time_in_seconds)
        return ans_time

    @staticmethod
    def convert_of_sec(time):
        if time > 0:
            hour = time // 3600
            minute = (time - hour * 3600) // 60
            second = time - hour * 3600 - minute * 60
            time_list = [str(hour), str(minute), str(second)]
            full_time = ':'.join(time_list)
        else:
            full_time = 0
        return full_time

    @staticmethod
    def convert_to_sec(time):
        if len(time) > 0:
            hour, minute, second = time.split(':')
            full_time = int(hour) * 3600 + int(minute) * 60 + int(second)
        else:
            full_time = 0
        return full_time


if __name__ == '__main__':
    path_to = os.getcwd()
    path = os.path.join(path_to, 'ex1.csv')
    ex1 = ScheduleAnalysis(path)
    all = 0
    for i in range(15):
        ex_average = ex1.convert_to_average(i)
        all += ex_average
        ex1.add_to_avr_struct(i, ex_average)
    print(all)
    print(ex1.sec_to_time('12:18:50', 90))
