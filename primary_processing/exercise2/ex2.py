import os
import pandas as pd


class DataInfo2(object):
    def __init__(self, path_to_csv, name):
        self.quantiles = {}
        self.data_info = []
        self.month_data = {}
        self.frequency = []
        df = pd.read_csv(path_to_csv, encoding='utf8', error_bad_lines=False, sep=';')
        self.extract_data = pd.DataFrame(df.loc[df['Name'] == name])
        print(self.extract_data)
        print(self.extract_data.describe())

    def get_quantile(self):
        self.quantiles['q25'] = self.extract_data['NumberOfPersons'].quantile(q=0.25)
        self.quantiles['q5'] = self.extract_data['NumberOfPersons'].quantile(q=0.5)
        self.quantiles['q75'] = self.extract_data['NumberOfPersons'].quantile(q=0.75)
        self.quantiles['iqr'] = self.quantiles['q75'] - self.quantiles['q25']
        print(self.quantiles)

    def check_name(self):
        maxi = self.extract_data['NumberOfPersons'].max()
        mini = self.extract_data['NumberOfPersons'].min()
        t = self.quantiles['iqr'] * 1.5
        down = self.quantiles['q25'] - t
        up = self.quantiles['q75'] + t

        print('нижняя граница:', down, 'минимальное значение:', mini,
              'максимальное значение:', maxi, 'верхняя граница:', up)

        if mini < down or up < maxi:
            return False
        else:
            return True


if __name__ == '__main__':
    ans = {}
    path = os.getcwd()
    file_name = 'boys_utf8.csv'
    path_to_file = os.path.join(path, file_name)

    name_o = 'Олег'
    oleg = DataInfo2(path_to_file, name_o)
    oleg.get_quantile()
    ans[name_o] = oleg.check_name()

    name_p = 'Павел'
    pavel = DataInfo2(path_to_file, name_p)
    pavel.get_quantile()
    ans[name_p] = pavel.check_name()

    name_a = 'Антон'
    anton = DataInfo2(path_to_file, name_a)
    anton.get_quantile()
    ans[name_a] = anton.check_name()

    name_v = 'Виктор'
    victor = DataInfo2(path_to_file, name_v)
    victor.get_quantile()
    ans[name_v] = victor.check_name()

    # girls
    file_name_girls = 'girls_utf8.csv'
    path_to_file_girls = os.path.join(path, file_name_girls)

    name_l = 'Лидия'
    lidia = DataInfo2(path_to_file_girls, name_l)
    lidia.get_quantile()
    ans[name_l] = lidia.check_name()

    name_e = 'Эмилия'
    emily = DataInfo2(path_to_file_girls, name_e)
    emily.get_quantile()
    ans[name_e] = emily.check_name()

    name_s = 'София'
    sofia = DataInfo2(path_to_file_girls, name_s)
    sofia.get_quantile()
    ans[name_s] = sofia.check_name()

    print(ans)

